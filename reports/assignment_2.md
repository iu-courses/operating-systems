####Assignment 2
####Jack Langston

##Q1

The assignment exemplified the motivation for using semaphores and why they are
so important in designing a performant OS (vs using polling.)

Additionally, I learned how the atoi function was implemented in trying to debug
why the example input parser was not working. It was clearly not manipulating pointers
in the way that the example suggested, so I was able to dig into the internals and change
them to produce the intended behavior. (To be honest, the input parsing was definitely
the most challenging part of the assignment.)

##Q2

As alluded to above, the largest mistake was in trying to get the atoi function to
work. I took the given example at face value (which obviously didn't work), and
spent much time chipping away at the code to understand the error and then increment
a revised atoi that provided the intended functionality.

Additionally, I had some trouble with optimizing the use of semaphores. My methodology
seems resources intensive as it utilizes two arrays of semaphores
(one "print" and one "done" for each process). I thought I could optimize it by just using
one shared semaphore but ran into some issues with race conditions. I think I can
do it with two semaphores (one "print" and one "done"), but I still have some concerns
about how much processor time is given to each process in each time slice and how that
might impact potential race conditions. I plan to do more research and implement a better
solution (if possible), but I wanted to make sure that this was submitted before the deadline.


##Q3

I went about the assignment by just jumping into the code (after reading the opening)
documentation. I think that if I were to do it again, I would have started by reading the
relevant sections of the textbook (e.g., semaphores and message passing) because
now after reading them I have a better understanding of the concepts and
some ideas about how I could improve the efficiency of my algorithms.
