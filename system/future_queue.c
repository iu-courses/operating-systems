#include "../include/xinu.h"


//creates a new future queue node
queue* future_alloc_q(pid32 pid, int* inbox ){

    queue* q  = (queue*) getmem(sizeof(queue));

    if((int32) q == SYSERR)
        return NULL;

    q->pid = pid;
    q->inbox = inbox;
    q->next = NULL;
    return q;
}

//deallocates the memory for the queue node
syscall future_free_q(queue* q){
    return freemem((char *) q, sizeof(queue));
}

//returns the new head of the queue list after trying to add a new q entry with pid
queue* future_enqueue(queue* q, pid32 pid, int* inbox){

    if(q == NULL)
        return future_alloc_q(pid, inbox);

    queue* head = q;
    while(q->next != NULL)
        q = q->next;
    q->next = future_alloc_q(pid, inbox);
    return head;
}

//returns the new head of the queue after trying to remove the first node
queue* future_dequeue(queue* q) {
    queue* next = q->next;
    if(future_free_q(q) == OK){
        return next;
    }else{
        printf("DQ fail\n");
        return q;
    }
}