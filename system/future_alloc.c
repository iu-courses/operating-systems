#include "../include/xinu.h"

/* Returns a pointer to an empty future with the given flag.
 */

future* future_alloc(int future_flag){
    intmask	mask = disable();

    future* new_future = (future *) getmem(sizeof(future));

    if (new_future == (future*) SYSERR){
        restore(mask);
        return NULL;
    }

    new_future->value = NULL;
    new_future->flag = future_flag;
    new_future->state = FUTURE_EMPTY;
    new_future->get_queue = NULL;
    new_future->set_queue = NULL;

    restore(mask);
    return new_future;
}
