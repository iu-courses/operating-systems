#include "../include/xinu.h"

syscall future_set(future* f, int* value) {
    intmask mask = disable();
    pid32 nextConsumer;

    switch (f->flag){

        case FUTURE_EXCLUSIVE:

            switch (f->state) {

                case FUTURE_VALID:
                    restore(mask);
                    return SYSERR;

                case FUTURE_WAITING:
                    f->state = FUTURE_VALID;
                    f->value = *value;
                    ready(f->pid);
                    break;

                case FUTURE_EMPTY:
                    f->state = FUTURE_VALID;
                    f->value = *value;
                    break;
            }
            break;

        case FUTURE_SHARED:

            switch (f->state) {

                case FUTURE_VALID:
                    restore(mask);
                    return SYSERR;

                case FUTURE_WAITING:

                    //stop rescheduling until we drain the queue
                    resched_cntl(DEFER_START);
                    int count = 0;
                    while(f->get_queue != NULL) {
                        count++;
                        //give the value to the first consumer
                        *(f->get_queue->inbox) = *value;

                        //remove the consumer from the queue
                        ready(f->get_queue->pid);
                        f->get_queue = future_dequeue(f->get_queue);
                    }
                    f->state = FUTURE_VALID;
                    f->value = *value;

                    //cant just fall through because this will activate a resched
                    resched_cntl(DEFER_STOP);
                    break;

                case FUTURE_EMPTY:
                    f->state = FUTURE_VALID;
                    f->value = *value;
                    break;
            }
            break;

        case FUTURE_QUEUE:

            switch (f->state) {

                case FUTURE_EMPTY:
                    f->state = FUTURE_VALID;
                    //fall through

                case FUTURE_VALID:

                    //put self on the setter queue
                    f->set_queue = future_enqueue(f->set_queue, getpid(), value);

                    //suspend self
                    (&proctab[getpid()])->prstate = PR_SUSP;
                    resched();
                    break;

                case FUTURE_WAITING:

                    //give the value to the first consumer
                    *(f->get_queue->inbox) = *value;

                    //remove the consumer from the queue
                    nextConsumer = f->get_queue->pid;
                    f->get_queue = future_dequeue(f->get_queue);

                    //if the get queue is now empty, then the future is empty
                    if(f->get_queue == NULL)
                        f->state = FUTURE_EMPTY;

                    //mark as ready
                    ready(nextConsumer);
                    break;
            }
            break;

    }

    restore(mask);
    return OK;

}
