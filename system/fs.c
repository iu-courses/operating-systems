
#include "../include/xinu.h"
#include <kernel.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#include "../include/fs.h"

/***********************************************************************************
 * INITIALIZATION AND DECLARATION
 ***********************************************************************************/

/*Set up the global definitions*/
#define SB_BLK 0
#define BM_BLK 1
#define RT_BLK 2
#define NUM_FD 16
#define INODES_PER_BLOCK (fsd.blocksz / sizeof(struct inode))
#define NUM_INODE_BLOCKS (( (fsd.ninodes % INODES_PER_BLOCK) == 0) ? fsd.ninodes / INODES_PER_BLOCK : (fsd.ninodes / INODES_PER_BLOCK) + 1)
#define FIRST_INODE_BLOCK 2

/*Set up the global variables*/
static struct fsystem fsd;
int dev0_numblocks;
int dev0_blocksize;
char *dev0_blocks;
extern int dev0;
struct filetable oft[NUM_FD];


/*Internal helper headers*/
int fs_fileblock_to_diskblock(int dev, int fd, int fileblock);
int fs_get_inode_by_num(int dev, int inode_number, struct inode *in);
int get_next_ft_slot();
int get_next_free_data_block();
bool valid_fd(int fd);
status move_to_last_directory_and_inode(char** file_name_ptr, struct directory* dir_ptr, struct inode* inode_ptr);
void _print_dir_helper(int depth, struct directory dir);

/*!
 * Sets up the superblock and bitmask block and saves them to dev0.
 *
 * Note:
 *
 *  -Currently only dev=0 is supported; must call bs_mkdev before this as this function
 *  utilizes global variables and functions that will only work after the the space for dev0
 *  has been allocated.
 *
 * @param dev
 * @param num_inodes
 * @return
 */
int fs_mkfs(int dev, int num_inodes) {
    int i;

    if (dev == 0) {
        fsd.nblocks = dev0_numblocks;
        fsd.blocksz = dev0_blocksize;
    }
    else {
        printf("Unsupported device\n");
        return SYSERR;
    }

    if (num_inodes < 1) {
        fsd.ninodes = DEFAULT_NUM_INODES;
    }
    else {
        fsd.ninodes = num_inodes;
    }

    i = fsd.nblocks;
    while ( (i % 8) != 0) {i++;}
    fsd.freemaskbytes = i / 8;

    if ((fsd.freemask = getmem(fsd.freemaskbytes)) == (void *)SYSERR) {
        printf("fs_mkfs memget failed.\n");
        return SYSERR;
    }

    /* zero the free mask */
    for(i=0;i<fsd.freemaskbytes;i++) {
        fsd.freemask[i] = '\0';
    }

    fsd.inodes_used = 0;

    /* write the fsystem block to SB_BLK, mark block used */
    fs_setmaskbit(SB_BLK);
    bs_bwrite(dev0, SB_BLK, 0, &fsd, sizeof(struct fsystem));

    /* write the free block bitmask in BM_BLK, mark block used */
    fs_setmaskbit(BM_BLK);
    bs_bwrite(dev0, BM_BLK, 0, fsd.freemask, fsd.freemaskbytes);

    return 1;
}


/***********************************************************************************
 * OPENING, CLOSING, and CREATING FILES
 ***********************************************************************************/


/*!
 * Opens a file descriptor for the indicated file and returns it.
 *
 * Will throw a SYSERR if:
 *
 * - The filename does not lead to an inode ("/" represents changing directories); always starts
 * at the root directory.
 * - there are no more file descriptors available
 *
 * Note: currently, flags are not supported.
 *
 *
 * @param filename
 * @param flags
 * @return an open file descriptor | SYSERR
 */

int fs_open(char *filename, int flags) {


    intmask mask = disable();

    char *curChar = filename;

    //ignore the root slash (if applicable)
    if (*curChar == '/')
        curChar++;

    //create the file descriptor
    int ft_slot = get_next_ft_slot();
    if(ft_slot == SYSERR){
        printf("fs_open: no available open file desriptors for the new file\n");
        goto failure;
    }
    struct filetable* ftent = &oft[ft_slot];



    //traverse the existing part of the path
    struct directory curDir;
    if(move_to_last_directory_and_inode(&curChar, &curDir, &ftent->in) == SYSERR){
        printf("fs_open: invalid file path %s\n", filename);
        goto failure;
    }

    //make sure it is a file
    if(*curChar != 0 || ftent->in.id == -1 || ftent->in.type != INODE_TYPE_FILE){
        printf("fs_open: not a file!\n");
        goto failure;
    }

    //get the directory entry -- TODO seems a bit redundant, should maybe alter move_to_last_directory_and_inode
    struct dirent *dirent_ptr = (struct dirent *) getmem(sizeof(struct dirent));
    for(int i = 0; i < curDir.numentries; i++){
        if(curDir.entry[i].inode_num == ftent->in.id){
            *dirent_ptr = curDir.entry[i];
            break;
        }
    }

    //set up the rest of the fd
    ftent->de = dirent_ptr;
    ftent->state = FSTATE_OPEN;
    ftent->fileptr = 0;

    restore(mask);
    return ft_slot;


failure:
    fs_close(ft_slot);
    restore(mask);
    return SYSERR;

}

/*!
 * Closes the given file descriptor (fd)
 *
 * Will throw SYSERR if:
 *
 *  - given an invalid file descriptor
 *
 * @param fd
 * @return
 */


int fs_close(int fd) {

    //disable interrupts
    intmask mask = disable();

    //get the file from the open file table
    if (!valid_fd(fd)){
        printf("fs_close: invalid file descriptor (%d)!\n", fd);
        restore(mask);
        return SYSERR;
    }

    //change the state to closed
    oft[fd].state = FSTATE_CLOSED;

    restore(mask);
    return OK;
}


/*!
 * Creates a new file inode in the system based on the indicated filename path. The filename path will always start
 * from the root (no relative addressing), and if the inodes required by the path do not exist, they will be created on
 * the fly (no mkdir needed). The created file will contain no pre-allocated data blocks (in case we eventually just
 * want to use the file descriptor for something else). It will, however, be automatically opened and placed in the
 * open file table (oft); the function returns the file descriptor (index) for the oft.
 *
 * fs_create does its best to be an atomic operation (i.e., if it fails, it leaves the file system in the same state
 * as when it started); however, one part of the operation cannot be done efficiently as a pre-check step: checking to see
 * if deeply nested directories have enough space to insert a new inode. This should be revisited, but the current side-effects
 * are not too terrible -- just creating some empty directories that you likely wanted to create in the first place.
 *
 * There are several ways that the fs_create call can fail:
 *
 * - invalid filename (double '/' or empty filename like /something/)
 * - trying to create a file that already exists
 * - running out of inode or data block space for the necessary allocations
 * - no available space in the open file table
 * - not enough space in a given directory for a new directory entry
 * - some sort of corruption in the file system that prevents reading and writing from the underlying data blocks
 *
 * @param filename - '/' is the ONLY way to represent a directory change; a leading '/' is optional
 * @param mode
 * @return SYSERR | file descriptor to the open file in the oft
 */

int fs_create(char *filename, int mode) {

    intmask mask = disable();

    char *curChar = filename;

    //ignore the root slash (if applicable)
    if (*curChar == '/')
        curChar++;


    //create the file descriptor
    int ft_slot = get_next_ft_slot();
    if(ft_slot == SYSERR){
        printf("fs_create: no available open file desriptors for the new file");
        goto failure;
    }
    struct filetable* ftent = &oft[ft_slot];


    struct inode curNode;
    struct directory curDir;

    //traverse the existing part of the path
    if(move_to_last_directory_and_inode(&curChar, &curDir, &curNode) == SYSERR){
        printf("fs_create: invalid file path %s\n", filename);
        goto failure;
    }

    //if the last node was a file, then the file already exists
    if(curNode.id != - 1 && curNode.type == INODE_TYPE_FILE){
        printf("fs_create: file already exists. Try fs_open.\n");
        goto failure;
    }

    //if there is no more to the file name, then we dont have a file name
    if(*filename == 0){
        printf("fs_create: no file specified.\n");
        goto failure;
    }


    //calculate the number of new inodes needed and check that there are no double slashes
    int numNewInodes = 0;
    bool8 last_was_slash = FALSE;
    for(char* temp = curChar; *temp != 0; ++temp) {
        if (*temp == '/') {
            if (last_was_slash) {
                printf("fs_create: invalid file path %s\n.", filename);
                goto failure;
            }
            numNewInodes++;
            last_was_slash = TRUE;
        }else{
            last_was_slash = FALSE;
        }
    }
    if(last_was_slash){
        printf("fs_create: file has no name! %s\n.", filename);
        goto failure;
    }
    numNewInodes++;


    if(fsd.inodes_used + numNewInodes >= fsd.ninodes){
        printf("fs_create: need to create more new inodes than are available.\n");
        goto failure;
    }


    //get the datablocks for the directories
    int datablocks_for_directories[10];
    int newDirNum = 0;
    for(int i = 0; i < numNewInodes - 1; i++){

        int dataBlock = get_next_free_data_block();
        if(dataBlock == SYSERR) {
            printf("fs_create: not enough free data blocks to store intermediate directories.\n");

            //unwind the allocated data blocks
            for(int j = 0; j < i; j++)
                fs_clearmaskbit(datablocks_for_directories[j]);

            goto failure;
        }
        datablocks_for_directories[i] = dataBlock;
    }


    printf("Creating rest of path %s\n", curChar);
    char* firstChar = curChar;
    while(TRUE){

        //check to make sure the current directory can take an additional entry
        //TODO this is the only thing that prevents the create mechanism from being atomic
        if(curDir.numentries >= DIRECTORY_SIZE){
            printf("fs_create: directory (%s) cannot hold additional entries.\n", curChar);
            goto failure;
        }

        //we have reached the end of the a directory name, create the missing directory inode
        if(*curChar == '/'){

            //save the block for the current directory
            int curDirBlock = curNode.id == -1 ? SB_BLK : curNode.blocks[0];

            //create the new inode
            curNode.id = fsd.inodes_used++;
            curNode.type = INODE_TYPE_DIR;
            curNode.device = 0;
            curNode.size = sizeof(struct directory);
            curNode.blocks[0] = datablocks_for_directories[newDirNum++];

            //create the directory entry
            struct dirent dir_ent;
            dir_ent.inode_num = curNode.id;
            char* temp = dir_ent.name;
            while(*firstChar != '/')
                *(temp++) = *(firstChar++);
            *temp = '\0';
            curDir.entry[curDir.numentries++] = dir_ent;

            //save the directory data and the next directory inode
            fs_put_inode_by_num(0, curNode.id, &curNode);
            if(curDirBlock != 0) {
                bs_bwrite(0, curDirBlock, 0, &curDir, sizeof(struct directory));
            }else{
                fsd.root_dir = curDir;
                bs_bwrite(dev0, curDirBlock, 0, &fsd, sizeof(struct fsystem));
            }

            //local updates
            curDir.numentries = 0;
            curChar++;
            firstChar = curChar;

        }

        //we have reached the end of the file name -- do the final inode creation and return the fd
        else if(*curChar == 0){

            //save the block for the current directory
            int curDirBlock = curNode.id == -1 ? SB_BLK : curNode.blocks[0];

            //create the inode
            ftent->in.id = fsd.inodes_used++;
            ftent->in.type = INODE_TYPE_FILE;
            ftent->in.device = 0;
            ftent->in.size = 0;
            for(int i=0; i < INODEBLOCKS; i++)
                ftent->in.blocks[i] = -1;

            //create the directory entry
            struct dirent *dir_ent_ptr = (struct dirent *) getmem(sizeof(struct dirent));
            char* temp = dir_ent_ptr->name;
            while(*firstChar != 0)
                *(temp++) = *(firstChar++);
            *temp = '\0';
            dir_ent_ptr->inode_num = ftent->in.id;
            curDir.entry[curDir.numentries++] = *dir_ent_ptr;

            //set the file descriptor information
            ftent->de = dir_ent_ptr;
            ftent->state = FSTATE_OPEN;
            ftent->fileptr = 0;

            //save the inode and blocks
            fs_put_inode_by_num(0, ftent->in.id, &ftent->in);
            if(curDirBlock != 0) {
                bs_bwrite(0, curDirBlock, 0, &curDir, sizeof(struct directory));
            }else{
                fsd.root_dir = curDir;
                bs_bwrite(dev0, curDirBlock, 0, &fsd, sizeof(struct fsystem));
            }

            restore(mask);
            return ft_slot;

        }else{
            curChar++;
        }
    }

failure:
    fs_close(ft_slot);
    restore(mask);
    return SYSERR;
}


/*!
 *
 * Follows file_name_ptr's indicated directory structure until reaching nonexistant part of path
 *
 * Sets file_name_ptr's underlying pointer forward to the either the start of nonexistent path (or to '\0' if all of path exists)
 * Sets dir_ptr's underlying directory to the last reached directory
 * Sets inode_ptr's underlying inode to the last reached inode
 *
 * If *inode_ptr.id = -1, then the inode doesn't exist and the directory is in the superblock
 * If *inode_ptr.type == INODE_TYPE_DIR, then *dir_ptr is that inode's directory data.
 * If *inode_ptr.type == INODE_TYPE_FILE, then *dir_ptr is the directory that contains a dirent for that inode.
 *
 * Returns SYSERR if:
 *
 *  - getting an inode or data block fails
 *  - the file_name tries to treat a file inode as a directroy inode
 *  - the file_name contains a double /
 *
 * Note: This may seem involved, but it should make sure that every data block and inode is loaded only once when
 * traversing file paths.
 *
 * @param file_name_ptr
 * @param dir_ptr
 * @param inode_ptr
 * @return OK | SYSERR
 */

status move_to_last_directory_and_inode(char** file_name_ptr, struct directory* dir_ptr, struct inode* inode_ptr){

    struct directory curDir = fsd.root_dir;
    struct inode curNode;

    curNode.id = -1;
    char *curDirChar, *curChar = *file_name_ptr, *firstChar = curChar;

    //move curDir to the proper directory and curChar to the segment of the filename path that still needs to
    //be created
    for(int i = 0; i < curDir.numentries; i++){

        curDirChar = curDir.entry[i].name;

        //move the pointers forward until one terminates or they are not equal
        while(*curDirChar != 0 && *curChar != 0 && *curChar == *curDirChar){
            curChar++;
            curDirChar++;
        }

        //check if we successfully matched the entire curDirChar
        if(*curDirChar == 0){

            //if we end up at a slash, then move directories
            if(*curChar == '/'){

                //get the inode of the directory entry
                if(fs_get_inode_by_num(0,curDir.entry[i].inode_num , &curNode) == SYSERR){
                    printf("move_to_last_directory_and_inode: tried to get invalid inode!\n");
                    return SYSERR;
                }

                //load the next directory
                if(curNode.type == INODE_TYPE_DIR && curNode.size == sizeof(struct directory)){

                    if(bs_bread(0, curNode.blocks[0], 0, &curDir, sizeof(struct directory)) == SYSERR){
                        printf("move_to_last_directory_and_inode: tried to load invalid data block!\n");
                        return SYSERR;
                    }
                    //cant have two slashes
                    if(*(++curChar) == '/') {
                        printf("move_to_last_directory_and_inode: invalid file name %s!\n", *file_name_ptr);
                        return SYSERR;
                    }
                    firstChar = curChar;
                    i = -1;
                    continue;

                    //if curNode is not a directory, then fail because we tried to treat file as directory
                }else{
                    printf("move_to_last_directory_and_inode: Tried to treat file inode as directory!\n");
                    return SYSERR;
                }

                //if we end up finished with file name, then we are done
            }else if(*curChar == 0) {

                //get the inode of the file entry
                if (fs_get_inode_by_num(0, curDir.entry[i].inode_num, &curNode) == SYSERR) {
                    printf("move_to_last_directory_and_inode: tried to get invalid inode!\n");
                    return SYSERR;
                }

                break;
            }
        }

        //if we didn't match the entire file name segment, restart and try again
        curChar = firstChar;
    }

    *file_name_ptr = curChar;
    *dir_ptr = curDir;
    *inode_ptr = curNode;
    return OK;

}



/***********************************************************************************
 * READING, WRITING, AND MOVING AROUND INSIDE FILES
 ***********************************************************************************/

/*!
 * Moves the cursor position of the given file descriptor by the given offset (in bytes).
 *
 * Returns SYSERR if:
 *
 * - fd does not represent an open file
 * - trying to move cursor past the beginning or end of the file
 *
 * Does NOT move cursor on SYSERR
 *
 * @param fd
 * @param offset
 * @return SYSERR | new cursor position
 */

int fs_seek(int fd, int offset) {

    //disable interrupts
    intmask mask = disable();

    //get the file from the open file table
    if (!valid_fd(fd)){
        printf("fs_seek: invalid file descriptor (%d)!\n", fd);
        goto failure;
    }

    struct filetable *file = &oft[fd];

    int newPosition = file->fileptr + offset;

    //check to make sure the new position is valid
    if(newPosition < 0){
        printf("fs_seek: Tried to seek to a negative index (%d).\n", newPosition);
        goto failure;
    }else if(newPosition > file->in.size){
        printf("fs_seek: Tried to seek to an index (%d) greater than the file size (%d).\n", newPosition, file->in.size);
        goto failure;
    }

    //save the new position
    file->fileptr = newPosition;

    restore(mask);
    return newPosition;

failure:
    restore(mask);
    return SYSERR;

}

/*!
 * Reads nbytes from the file indicated by the given file descriptor (fd) into buf. Starts reading at the current
 * cursor position and advances the cursor to one past the last byte read.
 *
 * Returns SYSERR if:
 *
 * - fd does not represent an open file
 * - trys to read pack the end of the file
 * - datablock corruption
 *
 * On SYSERR, does NOT advance the file cursor.
 *
 * @param fd
 * @param buf
 * @param nbytes
 * @return SYSERR | new cursor position
 */
int fs_read(int fd, void *buf, int nbytes) {

    //disable interrupts
    intmask mask = disable();

    //get the file from the open file table
    if (!valid_fd(fd)){
        printf("fs_read: invalid file descriptor (%d)!\n", fd);
        goto failure;
    }
    struct filetable *file = &oft[fd];

    if(file->fileptr + nbytes > file->in.size){
        printf("fs_read: Trying to read past the end of the file! size (%d) <= lastReadByte (%d)!\n", file->in.size, file->fileptr + nbytes - 1);
        goto failure;
    }

    //starting positions
    int curBlock = file->fileptr / MDEV_BLOCK_SIZE;
    int curOffset = file->fileptr - (curBlock * MDEV_BLOCK_SIZE);
    int bytesRemaining = nbytes;

    //while there are still bytes to read
    while(bytesRemaining > 0){

        //get the number of bytes to read
        int bytesLeftOnBlock = MDEV_BLOCK_SIZE - curOffset;
        int bytesToRead = bytesLeftOnBlock < bytesRemaining ? bytesLeftOnBlock : bytesRemaining;

        int diskBlock = oft[fd].in.blocks[curBlock];
        if(diskBlock == -1){
            printf("fs_read: Trying to read from nonexistant data block.\n");
            goto failure;
        }

        //read from block
        if(bs_bread(0,diskBlock,curOffset, buf, bytesToRead) == SYSERR){
            printf("fs_read: Could not read from indicated diskblock (%d) at given offset (%d) and byte to read (%d).", diskBlock, curOffset, bytesToRead);
            goto failure;
        };

        //make the necessary local updates
        curBlock++;
        bytesRemaining -= bytesToRead;
        curOffset = 0;
        buf += bytesToRead;
    }

    //global updates
    file->fileptr += nbytes;

    restore(mask);
    return file->fileptr;

failure:
    restore(mask);
    return SYSERR;

}

/*!
 * Writes nbytes from buf to the file represented by the given file descriptor (fd); moves to cursor to the end
 * of the bytes written to the file.
 *
 *
 * Returns SYSERR if:
 *
 *  - fd represents a valid, open file.
 *  - the number of bytes wont move the cursor past the max files size (MDEV_BLCOK_SIZE * INODEBLOCKS)
 *  - if new data blocks are needed, there are data blocks available
 *
 * On SYSERR, does NOT advance the file cursor.
 *
 * @param fd
 * @param buf
 * @param nbytes
 * @return SYSERR | the new cursor position
 */

int fs_write(int fd, void *buf, int nbytes) {

    //disable interrupts
    intmask mask = disable();

    //get the file from the open file table
    if (!valid_fd(fd)){
        printf("fs_write: invalid file descriptor (%d)!\n", fd);
        goto failure;
    }
    struct filetable *file = &oft[fd];

    if(file->fileptr + nbytes > MDEV_BLOCK_SIZE * INODEBLOCKS){
        printf("fs_write: Trying to write past max file size! max size (%d) < lastWriteByte (%d)!\n", MDEV_BLOCK_SIZE * INODEBLOCKS, file->fileptr + nbytes);
        goto failure;
    }

    //marker for if we allocate a new data block and need to save the inode again
    bool8 saveInode = FALSE;

    //starting positions
    int curBlock = file->fileptr / MDEV_BLOCK_SIZE;
    int curOffset = file->fileptr - (curBlock * MDEV_BLOCK_SIZE);
    int bytesRemaining = nbytes;

    //while there are still bytes to write
    while(bytesRemaining > 0){

        //get the number of bytes to write
        int bytesLeftOnBlock = MDEV_BLOCK_SIZE - curOffset;
        int bytesToWrite = bytesLeftOnBlock < bytesRemaining ? bytesLeftOnBlock : bytesRemaining;

        //get the physical block for the data (allocate if necessary)
        int diskBlock = oft[fd].in.blocks[curBlock];
        if(diskBlock == -1){
            diskBlock = get_next_free_data_block();

            if(diskBlock == SYSERR){
                printf("fs_write: No more data blocks available to write to!\n");
                goto failure;
            }

            //save the block
            file->in.blocks[curBlock] = diskBlock;
            saveInode = TRUE;
        }

        //write to the block
        if(bs_bwrite(0,diskBlock,curOffset, buf, bytesToWrite) == SYSERR){
            printf("fs_write: Could not write to indicated diskblock (%d) at given offset (%d) and byte to read (%d).", diskBlock, curOffset, bytesToWrite);
            goto failure;
        };

        //make the necessary local updates
        curBlock++;
        bytesRemaining -= bytesToWrite;
        curOffset = 0;
        buf += bytesToWrite;
    }

    //update the global data
    file->fileptr += nbytes;
    if(file->in.size < file->fileptr){
        file->in.size = file->fileptr;
        fs_put_inode_by_num(0,file->in.id,&(file->in));
    }else if(saveInode){
        fs_put_inode_by_num(0,file->in.id,&(file->in));
    }

    restore(mask);
    return file->fileptr;

failure:
    restore(mask);
    return SYSERR;

}

/***********************************************************************************
 * OPEN FILE TABLE UTILITIES
 ***********************************************************************************/

/*!
 * @param fd
 * @return TRUE iff the fd represents an open file in the oft
 */
bool valid_fd(int fd){
    return !(fd < 0 || fd >= NUM_FD || oft[fd].state == FSTATE_CLOSED);
}

/*!
 * Gives the next available fd. Additionally, performs memory cleanup of the old fds.
 *
 * @return the next fd or SYSERR if no fds are available
 */

int get_next_ft_slot() {
    static int nextSlot = 0;

    int start = nextSlot;
    do {
        struct filetable ftentry = oft[nextSlot];
        if (ftentry.state == FSTATE_CLOSED) {

            //free whatever was there before
            if (ftentry.de != NULL) {
                freemem((char *) ftentry.de, sizeof(struct dirent));
            }

            return nextSlot;
        }

        nextSlot++;
        if (nextSlot >= NUM_FD)
            nextSlot = 0;
    }while(nextSlot != start);

    return SYSERR;

}

/***********************************************************************************
 * INODE HELPERS // TODO - need to build efficient directory storage mechanism so we don't have to write the whole block
 *                  every time
 ***********************************************************************************/

/*!
 * Copy data from an inode fs block to the given inode struct; the particular inode is
 * determined by inode_number.
 *
 * Notes:
 *
 * - currently dev must always be 0
 *
 * Will throw SYSERR if given an invalid inode number.
 *
 * @param dev
 * @param inode_number
 * @param in
 * @return SYSERR | OK
 */
int fs_get_inode_by_num(int dev, int inode_number, struct inode *in) {
    int bl, inn;
    int inode_off;

    if (dev != 0) {
        printf("Unsupported device\n");
        return SYSERR;
    }
    if (inode_number > fsd.ninodes || inode_number < 0) {
        printf("fs_get_inode_by_num: inode %d out of range\n", inode_number);
        return SYSERR;
    }

    //get the block of the inode
    bl = inode_number / INODES_PER_BLOCK;
    bl += FIRST_INODE_BLOCK;

    //get the offset within the block
    inn = inode_number % INODES_PER_BLOCK;
    inode_off = inn * sizeof(struct inode);

    //copy the data from the inode block into the given inode struct
    bs_bread(dev0, bl, inode_off, in, sizeof(struct inode));

    return OK;

}

/*!
 * Copy data from given inode instruct into an inode fs block; the particular inode is
 * determined by inode_number.
 *
 * Notes:
 *
 * - currently dev must always be 0
 *
 * Will throw SYSERR if given an invalid inode number.
 *
 * @param dev
 * @param inode_number
 * @param in
 * @return SYSERR | OK
 */

int fs_put_inode_by_num(int dev, int inode_number, struct inode *in) {
    int bl, inn, inode_off;

    if (dev != 0) {
        printf("Unsupported device\n");
        return SYSERR;
    }
    if (inode_number > fsd.ninodes) {
        printf("fs_put_inode_by_num: inode %d out of range\n", inode_number);
        return SYSERR;
    }

    //get the block of the inode
    bl = inode_number / INODES_PER_BLOCK;
    bl += FIRST_INODE_BLOCK;

    //get the offset within the block
    inn = inode_number % INODES_PER_BLOCK;
    inode_off = inn * sizeof(struct inode);

    //write the inode to the block
    bs_bwrite(dev0, bl, inode_off, in, sizeof(struct inode));

    return OK;
}

/*!
 * Gives the next available datablock number; also marks the returned block as "in use" in the
 * bitmask
 *
 * @return next data block number | SYSERR iff no more data blocks are available
 */

int get_next_free_data_block(){
    static int nextblock = INODEBLOCKS + 3;
    int start = nextblock;
    do {
        if (!fs_getmaskbit(nextblock)) {
            fs_setmaskbit(nextblock);
            return nextblock;
        }

        nextblock++;
        if (nextblock >= MDEV_NUM_BLOCKS)
            nextblock = INODEBLOCKS + 3;
    }while(nextblock != start);

    return SYSERR;
}



/***********************************************************************************
 * Bitmask Helpers TODO These don't seem to save anything to the bitmap block
 ***********************************************************************************/

/* specify the block number to be set in the mask */
int fs_setmaskbit(int b) {
    int mbyte, mbit;
    mbyte = b / 8;
    mbit = b % 8;

    fsd.freemask[mbyte] |= (0x80 >> mbit);
    return OK;
}

/* specify the block number to be read in the mask */
int fs_getmaskbit(int b) {
    int mbyte, mbit;
    mbyte = b / 8;
    mbit = b % 8;

    return( ( (fsd.freemask[mbyte] << mbit) & 0x80 ) >> 7);
}

/* specify the block number to be unset in the mask */
int fs_clearmaskbit(int b) {
    int mbyte, mbit, invb;
    mbyte = b / 8;
    mbit = b % 8;

    invb = ~(0x80 >> mbit);
    invb &= 0xFF;

    fsd.freemask[mbyte] &= invb;
    return OK;
}


/***********************************************************************************
 * DEBUGGING
 ***********************************************************************************/
/* This is maybe a little overcomplicated since the lowest-numbered
   block is indicated in the high-order bit.  Shift the byte by j
   positions to make the match in bit7 (the 8th bit) and then shift
   that value 7 times to the low-order bit to print.  Yes, it could be
   the other way...  */
void fs_printfreemask(void) {
    int i,j;

    for (i=0; i < fsd.freemaskbytes; i++) {
        for (j=0; j < 8; j++) {
            printf("%d", ((fsd.freemask[i] << j) & 0x80) >> 7);
        }
        if ( (i % 8) == 7) {
            printf("\n");
        }
    }
    printf("\n");
}

/*!
 * Prints out some information about the file system.
 */
void fs_print_fsd(void) {

    printf("fsd.ninodes: %d\n", fsd.ninodes);
    printf("sizeof(struct inode): %d\n", sizeof(struct inode));
    printf("INODES_PER_BLOCK: %d\n", INODES_PER_BLOCK);
    printf("NUM_INODE_BLOCKS: %d\n", NUM_INODE_BLOCKS);
}

/*!
 * Prints out a directory map of what is stored in the blocks of the filesystem
 */
void fs_print_dir(void){
    intmask mask = disable();

    printf("Printing filesystem!\n");

    //read a fresh copy of the superblock from the underlying device (to make sure everything was properly committed)
    struct fsystem fs;
    bs_bread(0,SB_BLK,0, &fs, sizeof(struct fsystem));

    //recursively traverse the filesystem directories
    _print_dir_helper(0, fs.root_dir);

    restore(mask);
}

//private helper
void _print_dir_helper(int depth, struct directory dir){
    for(int i = 0; i < dir.numentries; i++){

        //print the name of the dir entry
        for(int j = 0; j < depth; j++)
            printf("- ");
        printf("%s\n", dir.entry[i].name);

        //get the inode of the directory entry
        struct inode node;
        if(fs_get_inode_by_num(0,dir.entry[i].inode_num , &node) == SYSERR){
            printf("fs_print_dir: tried to get invalid inode!\n");
            return;
        }

        struct directory newDir;

        //load the next directory (if applicable)
        if(node.type == INODE_TYPE_DIR && node.size == sizeof(struct directory)) {

            if (bs_bread(0, node.blocks[0], 0, &newDir, sizeof(struct directory)) == SYSERR) {
                printf("fs_print_dir: tried to load invalid data block!\n");
                return;
            }
            _print_dir_helper(depth + 1, newDir);
        }
    }
}
