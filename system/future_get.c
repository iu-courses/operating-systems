#include "../include/xinu.h"

syscall future_get(future* f, int* value){
    intmask mask = disable();
    pid32 nextProducer;

    switch(f->flag) {

        case FUTURE_EXCLUSIVE:

            switch (f->state) {

                case FUTURE_WAITING:
                    restore(mask);
                    return SYSERR;

                case FUTURE_EMPTY:
                    (&proctab[getpid()])->prstate = PR_SUSP;
                    f->pid = currpid;
                    f->state = FUTURE_WAITING;
                    resched();
                    //fall through to below

                case FUTURE_VALID:
                    *value = f->value;
                    f->state = FUTURE_EMPTY;
                    break;
            }
            break;

        case FUTURE_SHARED:

            switch (f->state) {

                case FUTURE_EMPTY:
                    f->state = FUTURE_WAITING;
                    //fall through to below

                case FUTURE_WAITING:

                    //put self on the getter queue
                    f->get_queue = future_enqueue(f->get_queue, getpid(), value);

                    //suspend self
                    (&proctab[getpid()])->prstate = PR_SUSP;
                    resched();
                    break;

                case FUTURE_VALID:
                    *value = f->value;
                    break;
            }
            break;

        case FUTURE_QUEUE:

            switch (f->state) {

                case FUTURE_EMPTY:
                    f->state = FUTURE_WAITING;
                    //fall through to below

                case FUTURE_WAITING:

                    //put self on the getter queue
                    f->get_queue = future_enqueue(f->get_queue, getpid(), value);

                    //suspend self
                    (&proctab[getpid()])->prstate = PR_SUSP;
                    resched();
                    break;

                case FUTURE_VALID:

                    //get the value from the setter
                    *value = *(f->set_queue->inbox);

                    //remove setter from the queue
                    nextProducer = f->set_queue->pid;
                    f->set_queue = future_dequeue(f->set_queue);

                    //if the set queue is empty, then the future is empty
                    if(f->set_queue == NULL)
                        f->state = FUTURE_EMPTY;

                    //mark the nextProducer as ready
                    ready(nextProducer);
                    break;
            }
            break;
    }

    restore(mask);
    return OK;

}
