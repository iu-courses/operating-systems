#include "../include/xinu.h"

syscall future_free(future* f){

    intmask mask = disable();

    if(f->get_queue != NULL)
        future_free_q(f->get_queue);

    if(f->set_queue != NULL)
        future_free_q(f->set_queue);

    if(freemem((char*) f, sizeof(future)) == SYSERR){
        restore(mask);
        return SYSERR;
    }

    restore(mask);
    return OK;
}
