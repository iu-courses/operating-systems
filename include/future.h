#ifndef _FUTURE_H_
#define _FUTURE_H_

#include "xinu.h"

/* define states */
#define FUTURE_EMPTY	  0
#define FUTURE_WAITING 	  1
#define FUTURE_VALID 	  2

/* modes of operation for future*/
#define FUTURE_EXCLUSIVE  1
#define FUTURE_SHARED     2
#define FUTURE_QUEUE      3

typedef struct queue{
    pid32 pid;
    int* inbox;
    struct queue* next;
} queue;

typedef struct futent
{
   int value; //this should NOT be a pointer so that gets after the sets of a FUTURE_SHARED do not fail
   int flag;
   int state;
   pid32 pid;
   queue* set_queue;
   queue* get_queue;
} future;


/* Interface for system call */
future* future_alloc(int future_flags);
syscall future_free(future*);
syscall future_get(future*, int*);
syscall future_set(future*, int*);

//helpers

queue* future_alloc_q(pid32, int*);
syscall future_free_q(queue*);
queue* future_enqueue(queue*, pid32, int*);
queue* future_dequeue(queue*);

#endif /* _FUTURE_H_ */
