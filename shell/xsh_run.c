/* xsh_run.c - xsh_run */

#include "../include/xinu.h"
#include "../include/shprototypes.h"
#include <stdio.h>

/*------------------------------------------------------------------------
 * xhs_run - redirects commands
 *------------------------------------------------------------------------
 */
shellcmd xsh_run(int nargs, char *args[])
{

	if (nargs > 1 && strncmp(args[1], "future_test", 11) == 0) {
		xsh_future_test(nargs-1, args+1);
	}else{
		printf("No valid command given!\n");
	}

	return 0;
}
