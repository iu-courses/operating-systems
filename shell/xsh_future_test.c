#include <xinu.h>
#include "../include/xinu.h"
#include "../include/stdlib.h"

uint32 future_ring(future *in, future *out);
uint future_prod(future* fut,int n);
uint future_cons(future* fut);
void printStates(future** fs, int n);

int ffib(int n);
int zero = 0;
int one = 1;
future** fibfut;

/**
 * Test Futures
 */
shellcmd xsh_future_test(int32 nargs, char *args[])
{

    //ring test
    if (nargs == 2 && strncmp(args[1], "-r", 2) == 0) {
        printf("Producer/consumer process ring\n");

        int future_flags = FUTURE_EXCLUSIVE;
        int ring_count = 10;
        int final_val;
        int i;

        future *first, *in, *out = NULL;
        first = future_alloc(future_flags);
        in = first;
        for (i=0; i < ring_count; i++) {
            out = future_alloc(future_flags);
            resume( create(future_ring, 1024, 20, "", 2, in, out) );
            in = out;
        }
        printf("master sets %d\n", ring_count);
        future_set(first, &ring_count);
        future_get(out, &final_val);
        printf("master gets %d\n", final_val);
        return(OK);
    }

    //producer consumer test
    else if(nargs == 2 && strncmp(args[1], "-pc", 3) == 0){

        future* f_exclusive,
                * f_shared,
                * f_queue;

        f_exclusive = future_alloc(FUTURE_EXCLUSIVE);
        f_shared    = future_alloc(FUTURE_SHARED);
        f_queue     = future_alloc(FUTURE_QUEUE);

        // Test FUTURE_EXCLUSIVE
        resume( create(future_cons, 1024, 20, "fcons1", 1, f_exclusive) );
        resume( create(future_prod, 1024, 20, "fprod1", 2, f_exclusive, 1) );

        // Test FUTURE_SHARED
        resume( create(future_cons, 1024, 20, "fcons2", 1, f_shared) );
        resume( create(future_cons, 1024, 20, "fcons3", 1, f_shared) );
        resume( create(future_cons, 1024, 20, "fcons4", 1, f_shared) );
        resume( create(future_cons, 1024, 20, "fcons5", 1, f_shared) );
        resume( create(future_prod, 1024, 20, "fprod2", 2, f_shared, 2) );

        // Test FUTURE_QUEUE
        resume( create(future_cons, 1024, 20, "fcons6", 1, f_queue) );
        resume( create(future_cons, 1024, 20, "fcons7", 1, f_queue) );
        resume( create(future_cons, 1024, 20, "fcons8", 1, f_queue) );
        resume( create(future_cons, 1024, 20, "fcons9", 1, f_queue) );
        resume( create(future_prod, 1024, 20, "fprod3", 2, f_queue, 3) );
        resume( create(future_prod, 1024, 20, "fprod4", 2, f_queue, 4) );
        resume( create(future_prod, 1024, 20, "fprod5", 2, f_queue, 5) );
        resume( create(future_prod, 1024, 20, "fprod6", 2, f_queue, 6) );
        return(OK);
    }

    else if(nargs == 3 && strncmp(args[1], "-f", 2) == 0){

        int n = atoi(&args[2]);
        printf("Futures Fibonacci for N=%d\n", n);

        //init the future array
        fibfut = (future **) getmem(sizeof(future) * (n + 1));
        for(int i = 0; i <= n; i++)
            fibfut[i] = future_alloc(FUTURE_SHARED);

        //start the workers
        resched_cntl(DEFER_START);
        for(int i = 0; i <=n; i++)
            resume( create(ffib, 1024, 20, "ffib", 1, i));
        resched_cntl(DEFER_STOP);

        //get the answer
        int ans;
        future_get(fibfut[n], &ans);
        printf("Nth Fibonacci value for N=%d is %d\n", n, ans);

        //dealloc future array
        for(int i = 0; i <=n; i++)
            future_free(fibfut[i]);

        return(OK);
    }


    else {
        printf("No valid options\n");
        return(OK);
    }

}

void printStates(future** fs, int n){
    for(int i = 0; i <=n; i++){
        printf("%d : %d\n", i, (*(fs++))->state);
    }
}

uint future_ring(future *in, future *out) {
  int val;
  future_get(in, &val);
  printf("Process %d gets %d, puts %d\n", getpid(), val, val-1);
  val--;
  future_free(in);
  future_set(out, &val);
  return OK;
}

uint future_prod(future* fut, int n) {
    printf("Produced %d\n", n);
    future_set(fut, &n);
    return OK;
}

uint future_cons(future* fut) {
    int i, status;
    status = (int) future_get(fut, &i);
    if (status < 1) {
        printf("future_get failed\n");
        return -1;
    }
    printf("Consumed %d\n", i);
    return OK;
}

int ffib(int n) {

    int minus1 = 0;
    int minus2 = 0;
    int this = 0;

    if (n == 0) {
        future_set(fibfut[0], &zero);
        return OK;
    }

    if (n == 1) {
        future_set(fibfut[1], &one);
        return OK;
    }

    future_get(fibfut[n-2], &minus2);
    future_get(fibfut[n-1], &minus1);


    this = minus1 + minus2;
    future_set(fibfut[n], &this);
    return(0);

}
