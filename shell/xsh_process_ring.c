/* xsh_process_ring.c - xsh_process_ring */

#include <xinu.h>
#include <stdio.h>
#include <stdlib.h>
#define PROCESS_RING_INITVAL 2147483647
#define PROCESS_RING_DEFAULT_PROCESSNUM 2
#define PROCESS_RING_DEFAULT_ROUNDNUM 3

// 0 - poll
// 1 - sync
#define PROCESS_RING_DEFAULT_STATE 0


/*************************************************************************
process_ring Overview
----------------------------------------------------

This code launches a countdown process that passings responsbility for the countdown
amongst a "ring" of indepenent processes.

i.e., Process 0 prints the first value and hands responsibility to process 1;
Process 1 then prints the second value and hands the responsibility to process 2;
...
Process n then prints the n+1 value and hands responsibility to process 0.
...
The countdown terminates when the value hits 0.


implementation Details
----------------------------------------------------
The handoff of responsibility is implemented using two methodologies:

(1) polling (inefficient)
(2) semaphores (efficient)

These act as the "triggers" to act. When each process "acts", it:
(1) reads a value from its inbox
(2) prints the value (and some additional meta information)
(3) loads a value into its neighbors inbox
(4) triggers it's neighbor to act (with polling this is inherent)

Usage
----------------------------------------------------
process_ring <numRounds> <numProcesses>

Flags:

-r <numRounds>      : sets the number of rounds
-p <numProcesses>	: sets the number of processes
-i <poll/sync>      : poll implements polling, sync implements sempahores

Defaults:

2 processes, 3 rounds, polling

***************************************************************/



/*********************************************************
SEMAPHORE IMPLEMENTATION
*********************************************************/


/**
semaphore_process
----------------------------------------------------
The function that is the starting point for each sempahore process

myBox - a pointer to the inbox from which it extracts the values to be printed
nextBox - a pointer to its neighbor's inbox to which it loads the value for it's neighbor to print
mySem - the semaphore ID of the semaphore that triggers this processes print action
nextSem - the semaphore ID of the semaphore that triggers the next processes print action
doneSem - signaled when the process completes
processNum - the number of the process in the "process ring"
*/


process semaphore_process(volatile int32 *myBox, int32 *nextBox, sid32 mySem, sid32 nextSem, sid32 doneSem, uint8 processNum){
	int32 roundNum = 0, curVal;

	//continue until -1 in process inbox
	while(1){
		wait(mySem);
		if((curVal = *myBox) < 0)
			break;
		printf("Ring Element %d : Round %d : Value : %d\n",processNum, roundNum++, curVal);
		*nextBox = curVal - 1;
		signal(nextSem);
	}

	//on exit, put a -1 in neighbor's inbox -- this will cascade across the process ring
	*nextBox = -1;
	signal(nextSem);
	semdelete(nextSem);
	signal(doneSem);
	return OK;
}

/**
sync
----------------------------------------------------
The driver that creates and monitors the process_ring that uses semaphores

numProcesses - the number of processes in the "process ring"
inbox - a pointer to the array of inboxes (one for each process)
*/


void sync(uint8 numProcesses, volatile int32* inbox){

	sid32 printSemaphores[numProcesses], doneSemaphores[numProcesses];
	for(uint8 i = 0; i < numProcesses; i++){
		printSemaphores[i] = semcreate(0);
		doneSemaphores[i] = semcreate(0);
	}

	char processName[15];
	for(uint8 p = 0; p < numProcesses; p++){

		sprintf(processName, "process_%d", p);
		volatile int32* myBox = &inbox[p];
		volatile int32* nextBox = &inbox[(p + 1) % numProcesses];
		sid32 mySem = printSemaphores[p];
		sid32 nextSem = printSemaphores[(p + 1) % numProcesses];
		sid32 doneSem = doneSemaphores[p];

		resume(
			create(	semaphore_process,
					1024,
					20,
					processName,
					6,
					myBox,
					nextBox,
					mySem,
					nextSem,
					doneSem,
					p));

	}

	//kickoff
	signal(printSemaphores[0]);

	//synchronization
	for(uint8 p = 0; p < numProcesses; p++){
		wait(doneSemaphores[p]);
		semdelete(doneSemaphores[p]);
	}
}

/*********************************************************
POLLING IMPLEMENTATION
*********************************************************/

/**
polling_process
----------------------------------------------------
The function that is the starting point for each polling process

myBox - a pointer to the inbox from which it extracts the values to be printed
nextBox - a pointer to its neighbor's inbox to which it loads the value for it's neighbor to print
processNum - the number of the process in the "process ring"
*/

process polling_process(volatile int32 *myBox, int32 *nextBox, uint8 processNum){
	int32 lastVal = PROCESS_RING_INITVAL, roundNum = 0, curVal;

	//continue until -1 in process inbox
	while((curVal = *myBox) >= 0){

		//if the inbox has changed
		if(lastVal != curVal){
			printf("Ring Element %d : Round %d : Value : %d\n",processNum, roundNum++, curVal);
			lastVal = curVal;
			*nextBox = curVal - 1;
		}
	}

	//on exit, put a -1 in neighbor's inbox -- this will cascade across the process ring
	*nextBox = -1;
	return OK;
}


/**
poll
----------------------------------------------------
The driver that creates and monitors the process_ring that uses polling

numProcesses - the number of processes in the "process ring"
inbox - a pointer to the array of inboxes (one for each process)
*/


void poll(uint8 numProcesses, volatile int32* inbox){
	char processName[15];
	for(uint8 p = 0; p < numProcesses; p++){

		sprintf(processName, "process_%d", p);
		volatile int32* myBox = &inbox[p];
		volatile int32* nextBox = &inbox[(p + 1) % numProcesses];

		//TODO Figure out why we have to set the priority higher and how we know "20" is appropriate
		resume(create(polling_process, 1024, 20, processName, 3, myBox, nextBox, p));
	}


	//synchronization
	//when a child terminates, it puts a -1 in its inbox. don't terminate until
	//all inboxes have -1
	uint8 allDone = 0;
	while(!allDone){
		allDone = 1;
		for(uint8 p = 0; p < numProcesses; p++){
			if(inbox[p] != -1){
				allDone = 0;
				break;
			}
		}
	}


}


/*****************************************************************
USER INTERFACE
*****************************************************************/


/**
load_args
----------------------------------------------------
Reads the command line arguments/flags and assigns the appropriate values to the pointers passed in

nargs - number of command line arguments
args - pointer to the array of command line arguments
numProcsses - a pointer to the integer tracking the number of Processes
numRounds - a pointer to the integer tracking the number of Rounds
state - a pointer to the integer tracking the program state (0 - poll; 1 - sync)

*/

int load_args(int nargs, char** args, volatile uint8* numProcesses, volatile int32* numRounds, volatile uint8* state){

	//set the default values
	*numProcesses = PROCESS_RING_DEFAULT_PROCESSNUM;
	*numRounds = PROCESS_RING_DEFAULT_ROUNDNUM;
	*state = PROCESS_RING_DEFAULT_STATE;

	char *endp;
	uint8 p;
	int32 r, startNum = 1;

	//If the first arg is an integer, then set it to be the rounds
	if(nargs >= 2){
		endp = args[1];
		r = atoi(&endp);
		if (endp != args[1] && *endp == '\0') {
			*numRounds = r;
			startNum++;
		}
	}

	//If first and second args are integers, then take the second arg as the num processes
	if(nargs >= 3 && startNum == 2){
		endp = args[2];
		p = atoi(&endp);
		if (endp != args[2] && *endp == '\0') {
			*numProcesses = p;
			startNum++;
		}
	}


	//cycle through the args, skipping the first 1-2 if they are integers (see above)
	for(int i = startNum; i < nargs; i++){

		if(0 == strncmp("-p", args[i], 3)){

			// No argument after flag
			if (! (i + 1 < nargs)) {
				printf("-p flag expected an argument\n");
				return SHELL_ERROR;
			}

			endp = args[i+1];
			p = atoi(&endp);

			/* The end pointer hasn't advanced,
			 No number was parsed.*/
			if (endp == args[i + 1]) {
				printf("-p flag expected an integer\n");
				return SHELL_ERROR;

			/* There was trailing garbage in the string that
			wasn't converted to an integer. */
			} else if (*endp != '\0'){
				printf("-p recieved invalid integer\n");
				return SHELL_ERROR;

			/* The number is out of range */
			} else if (!(0 <= p && p <= 64)) {
				printf("-p flag expected a number between 0 - 64\n");
				return SHELL_ERROR;
			}

			*numProcesses = p;
			i++;

		}else if(0 == strncmp("-r", args[i], 3)){

			// No argument after flag
			if (! (i + 1 < nargs)) {
				printf("-r flag expected an argument\n");
				return SHELL_ERROR;
			}

			endp = args[i+1];
			p = atoi(&endp);

			/* The end pointer hasn't advanced,
			 No number was parsed.*/
			if (endp == args[i + 1]) {
				printf("-r flag expected an integer\n");
				return SHELL_ERROR;

			/* There was trailing garbage in the string that
			wasn't converted to an integer. */
			} else if (*endp != '\0'){
				printf("-r recieved invalid integer\n");
				return SHELL_ERROR;
			}

			*numRounds = r;
			i++;

		}else if(0 == strncmp("-i", args[i], 3)){

			// No argument after flag
			if (! (i + 1 < nargs)) {
				printf("-i flag expected `poll` or `sync` argument\n");
				return SHELL_ERROR;

			//correct arguments
			}else if(0 == strncmp("poll", args[i + 1], 5)){
				*state = 0;
			}else if(0 == strncmp("sync", args[i + 1], 5)){
				*state = 1;

			//invalid argument
			}else{
				printf("-i flag expected `poll` or `sync` argument\n");
				return SHELL_ERROR;
			}
			i++;

		}else{
			return SHELL_ERROR;
		}
	}

	return SHELL_OK;
}

/*****************************************************************
DRIVER
*****************************************************************/
/**
xsh_process_ring
----------------------------------------------------
Entry point for the shell command

*/

shellcmd xsh_process_ring(int nargs, char *args[])
{
	volatile uint8 numProcesses, state;
	volatile int32 numRounds;

	//load in the arguments and exit on error or --help
	int status = load_args(nargs, args, &numProcesses, &numRounds, &state);
	if(status == SHELL_ERROR){
		return status;
	}


	printf("Number of Processes: %d\n", numProcesses);
	printf("number of Rounds: %d\n", numRounds);

	//set up the inbox
	volatile int32 inbox[numProcesses];
	for(int i = 0; i < numProcesses; i++)
		inbox[numProcesses] = PROCESS_RING_INITVAL;
	inbox[0] = numProcesses * numRounds - 1;

	//initialize the timer
	uint32 startTime, endTime;
	gettime(&startTime);

	//start the countdown processes
	if(state == 0){
		poll(numProcesses, inbox);
	}else{
		sync(numProcesses, inbox);
	}



	//print out the timing stats
	gettime(&endTime);
	printf("Elapsed Seconds: %d\n", endTime - startTime);


	return SHELL_OK;
}
