/* xsh_echo.c - xsh_echo */

#include <xinu.h>
#include <stdio.h>

/*------------------------------------------------------------------------
 * xhs_echo - write argument strings to stdout
 *------------------------------------------------------------------------
 */
shellcmd xsh_hello(int nargs, char *args[])
{

	if (nargs != 2) {
		printf("Invalid number of arguments. Syntax: xsh$ hello <string>\n");
	}

	printf("Hello %s, Welcome to the world of Xinu!!\n", args[1]);

	return 0;
}
