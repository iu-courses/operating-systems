#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

int main(int nargs, char *args[]) {

    if(nargs != 2){
        printf("sys_info: Invalid Syntax! Should be $: sys_info <path_to_binary>\n");
        return -1;
    }

    int fd[2], childpid;
    pipe(fd);

    //fork the process
    if((childpid = fork()) == -1){
        perror("fork");
        return -1;
    }


    //child commands
    if(childpid == 0){
        printf("Child PID = %d \n", getpid());

        //read arg
        char readBuffer[80];
        close(fd[1]);
        read(fd[0], readBuffer, sizeof(readBuffer));

        //execute the argument
        if(strncmp(readBuffer, "/bin/echo\0" , 9) == 0){
            execl(readBuffer, readBuffer, "Hello World!\0", (char *) NULL);
        }else{
            execl(readBuffer, readBuffer, (char *) NULL);
        }

    //parent commands
    }else{
        printf("Parent PID = %d \n", getpid());

        //send arg
        close(fd[0]);
        write(fd[1], args[1], (strlen(args[1]) + 1));

        //wait for child to finish
        int status;
        wait(&status);

    }

    return 0;
}
